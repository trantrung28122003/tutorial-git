# Coffee Management #

| **2180608012 - Vương Khả Thạch** | |
| --- | --- |
| **Title** | User can log into the system successfully |
| **Value Statement** |  As a user, I want to log in to the system using my login information and access my work status. |
| **Acceptance Criteria** | |
| | **Scenario 1: Successful Login** |
| | Given I have a valid username and password, |
| | When I enter my credentials and click the "Log In" button, |
| | Then I should be redirected to the dashboard and see my job statuses. |
| | |
| | **Scenario 2: Unsuccessful Login** |
| | Given I have an invalid username or password, |
| | When I enter my credentials and click the "Log In" button, |
| | Then I should see an error message stating "Invalid username or password. Please try again." |
| | |
| | **Scenario 3: Empty Fields** |
| | Given I have left the username or password field empty, |
| | When I click the "Log In" button, |
| | Then I should see an error message stating "Please enter your username and password." |
| **Definition of Done** | |
| | - Code implementation per acceptance criteria. |
| | - Test cases passed. |
| | - User-friendly interface designed and implemented. |
| | - Security measures in place. |
| | - Usability and performance tested. |
| | - Documentation updated. |
| | - User acceptance tested and approved. |
| | - Accessibility, cross-browser, and cross-device compatibility guaranteed. |
| | - Performance monitoring tools implemented. |
| Owner | Vương Khả Thạch |
| Integration | 2 weeks |
| Estimate | 2 Points |


# Test Case #
| **Test Case Title** | Login Functionality Test |
| --- | --- |
| **Test Case ID** | TC_LG_01 |
| **Test Case Description** | This test case verifies the login functionality of the application. |
| **Preconditions** | The application is installed and running. The user has a valid username and password. |
| **Test Steps** | 
| | 1. Open the application.
| | 2. Click on the "Login" button.
| | 3. Enter a valid username and a valid password in the respective input fields.
| | 4. Click the "Login" button. |
| **Expected Results** | 
| | - Expected Result 1: The login form should open without errors.
| | - Expected Result 2: The user should be able to enter a valid username and password in the input fields.
| | - Expected Result 3: After clicking the "Login" button, the application should verify the credentials, and if they are correct, the user should be successfully logged in.
| | - Expected Result 4: A successful login should redirect the user to the main dashboard or homepage of the application.
| | - Expected Result 5: If the credentials are incorrect, an error message should be displayed, and the user should not be logged in. |
| **Postconditions** | The user is either successfully logged in or remains on the login page with an error message. |
| **Test Data** | 
| | - Valid username: "admin"
| | - Valid password: "123" |
| **Test Environment** | 
| | - Device: [Specify the device on which the test was conducted]
| | - Browser/Application Version: [Specify the version of the browser/application]
| | - Operating System: [Specify the OS and version]
| | - Network Connection: [Specify the network connection type] |
| **Test Verdict** | 
| | - The test case should be marked as "Pass" if all the expected results are achieved.
| | - The test case should be marked as "Fail" if any of the expected results are not achieved. |








![Alt text](https://gitlab.com/khathach699/tutorial-git/-/raw/main/image/GitLoggin.jpg?ref_type=heads)




# USER STORY 

# 2180608076- Kwon Sang Thuan #
| Title | Edit drink menu | 
| :----- | :---------- | 
| Value Stament      | As a user, I want to edit product prices, update images and record change history | 
| Acceptance Criteria       | -Classify drinks into different categories|
|                           | -Menu items can be turned on or off |
|                           | -There is a window displaying a notification when saving changes|
|                           | -Can change and add discount codes|
|                           | -All changes in prices and images are recorded in the history section      | 
| Definition of Done      |- All requested changes to the drink menu have been implemented.          | 
|                           | - The menu has been updated with the new drinks or modifications|
| Owner      | Kwon Sang Thuan          |
|Iteration  |   Unscheduled|


![UserStoryImage](https://gitlab.com/kwonsangthuan/8076/-/raw/main/%E1%BA%A2nh%20ch%E1%BB%A5p%20m%C3%A0n%20h%C3%ACnh%202023-10-09%20203613.png)




# 2180607425 - Nguyễn Thành Đạt #

# UserStory #
| **Title**               | Add User Account Into Account List                              |
| ----------------------- | ----------------------------------------------- |
| **Value Statement**     |   As a manager , i want to add new account for my staff ,so they can enter date of order that customer has ordered .
| **Acceptance Criteria** |   **_Acceptance Citerion :_** <br> Given that database has enough data space <br> When manager request to add new account <br> -> Then ensure that new data has been updated and display in list user.
| **Definition of Done**  |   * Acceptance Criteria Met <br> * Test Cases Passed <br> * Package Into Specific Folders<br>  * Show Users List <br>  * Code Reviewed<br> * Product Owner Accepts User Story
| **Owner**               |    Nguyễn Thành Đạt                             |
| **Iteration**             | Unsheduled                                           |
| **Estimate**              | 5 Points                                             |
    
![UserStoryImage](https://gitlab.com/huyhoang835784/baitapgitreal/-/raw/main/img/GitReadme.PNG?ref_type=heads)

